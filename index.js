const express = require('express')
const app = express()

app.use(
  express.urlencoded({
    extended: true
  })
)

app.use(express.json())

app.post('/', (req, res) => {
    var object = require('./objects/' + req.body.object);
    res.send(object[req.body.method](req.body.data));
})

app.listen(6968)